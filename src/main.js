import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

/*
Instanciamos nuestra aplicación Vue con:
* Vue Router
* Vuex
* Función para crear o renderizar nuestro componente raiz
finalmente la instancia de Vue se monta en un determinado elemento (app)
*/
new Vue({
    router,
    store,
    render: createElement => createElement(App)
}).$mount('#app')
